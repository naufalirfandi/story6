# Story 6
This Gitlab repository is for story 6,7,8,9,10 PPW

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/naufalirfandi/story6/badges/master/pipeline.svg)](https://gitlab.com/naufalirfandi/story6/commits/master)
[![coverage report](https://gitlab.com/naufalirfandi/story6/badges/master/coverage.svg)](https://gitlab.com/naufalirfandi/story6/commits/master)

## Heroku
Click [here](https://story6-naufal-hilmi-irfandi.herokuapp.com/) to see my website.