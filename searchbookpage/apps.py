from django.apps import AppConfig


class SearchbookpageConfig(AppConfig):
    name = 'searchbookpage'
