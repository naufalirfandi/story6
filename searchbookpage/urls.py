from django.contrib import admin
from django.urls import path
from .views import searchbookpage, data

urlpatterns = [
    path('', searchbookpage),
    path('data/', data),
]