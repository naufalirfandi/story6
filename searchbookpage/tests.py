from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from .views import searchbookpage, data
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class UnitTest(TestCase):

    def test_apakah_url_yang_diakses_ada(self):
        response = Client().get("/SearchBook/")
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_data_json_tersedia(self):
        response = Client().get("/SearchBook/data/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve("/SearchBook/")
        self.assertEqual(found.func, searchbookpage)
    
    def test_apakah_mengakses_function_page_json_yang_benar(self):
        found = resolve("/SearchBook/data/")
        self.assertEqual(found.func, data)

    def test_apakah_template_terpakai(self):
        response = Client().get("/SearchBook/")
        self.assertTemplateUsed(response, 'searchbookpage/searchbookpage.html')

class functionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome("./chromedriver" ,chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_website(self):
        self.browser.get(self.live_server_url + '/SearchBook/')
        self.assertIn("Search Book", self.browser.page_source)
        self.assertIn('<div class="table-responsive"', self.browser.page_source)
        time.sleep(3)
        self.browser.find_element_by_name('searchbar').send_keys('programming')
        time.sleep(5)
        self.assertIn('<td>', self.browser.page_source)
        time.sleep(3)