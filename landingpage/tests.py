from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from .views import landingpage
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class UnitTest(TestCase):

    def test_apakah_url_yang_diakses_ada(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve("/")
        self.assertEqual(found.func, landingpage)

class functionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome("./chromedriver" ,chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_websit(self):
        self.browser.get(self.live_server_url + '/')
        
        self.assertIn("Sedikit preview mengenai apa yang sedang saya lakukan dan pengalaman saya.", self.browser.page_source)
        self.assertIn("About Me, <b>Naufal</b> Hilmi Irfandi", self.browser.page_source)
        self.assertIn('<div class="dot" id="dot1"', self.browser.page_source)
        self.assertIn('<div class="dot" id="dot2"', self.browser.page_source)
        self.assertIn('<div class="accordionAyam"', self.browser.page_source)
        time.sleep(3)
        self.browser.find_element_by_name('accordion1').click()
        self.assertIn("Nugas", self.browser.page_source)
        time.sleep(3)
        self.browser.find_element_by_name('accordion2').click()
        time.sleep(3)
        self.assertIn("Bem Fasilkom UI", self.browser.page_source)
        time.sleep(3)
        self.browser.find_element_by_name('accordion3').click()
        time.sleep(3)
        self.assertIn("Staff DRM Compfes", self.browser.page_source)
        time.sleep(3)
        self.browser.find_element_by_name('accordion3').click()
        time.sleep(3)
        self.browser.find_element_by_name('dark').click()
        time.sleep(3)
        bg = self.browser.find_element_by_name('core').value_of_css_property('background-color')
        self.assertIn('rgba(53, 58, 64, 1)', bg)
        time.sleep(3)
        self.browser.find_element_by_name('light').click()
        time.sleep(3)
        bg = self.browser.find_element_by_name('core').value_of_css_property('background-color')
        self.assertIn('rgba(248, 249, 251, 1)', bg)


# Create your tests here.
