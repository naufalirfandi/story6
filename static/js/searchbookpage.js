$(document).ready(function() {
    $("#textField").on("keyup", function(e) {
        q = e.currentTarget.value.toLowerCase()
        console.log(q)

        $.ajax({
            url: "data/?q=" + q,
            datatype: 'json',
            success: function(data) {
                $('#hasilSearch').html('')
                var result = '<tr>';
                for (var i = 0; i < data.items.length; i++) {
                    //bagian ini di challenge reflection sudah ketemu problemnya adalah salah nama variabel
                    result += "<tr> <th scope='row'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" +
                        data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td>" + data.items[i].volumeInfo.publishedDate + "</td>";
                }
                $('#hasilSearch').append(result);
            }
        })
    });
});