from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login,logout

# Create your views here.
def signup_views(request):
    if request.method == "POST":
        formSignUp = UserCreationForm(request.POST)
        if formSignUp.is_valid():
            user = formSignUp.save()
            login(request,user)
            # log user in
            return redirect('/Kabar!/')
    else:
        formSignUp = UserCreationForm()
    return render(request, 'accounts/signup.html', {'formSignUp' : formSignUp})

def signin_views(request):
    if request.method == "POST":
        formSignIn = AuthenticationForm(data = request.POST)
        if formSignIn.is_valid():
            # log in user
            if 'next' in request.POST:
                user = formSignIn.get_user()
                login(request, user)
                return redirect(request.POST.get("next"))
            else:
                return redirect('/')
    else:
        formSignIn = AuthenticationForm()
    return render(request, 'accounts/signin.html', {'formSignIn' : formSignIn})

def signout_views(request):
    if request.method == "POST":
        logout(request)
        return redirect('/Kabar!/')