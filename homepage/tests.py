from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.contrib.auth.models import User
from django.utils import timezone
from .views import homepage
from .models import coreDatabase
from .forms import formKabar
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class UnitTest(TestCase):

    def setUp(self):
        # Create two users
        test_user1 = User.objects.create_user(username='testuser1', password='1X<ISRUkw+tuK')
        test_user2 = User.objects.create_user(username='testuser2', password='2HJ1vRV0Z&3iD')
        
        test_user1.save()
        test_user2.save()

    def test_apakah_url_yang_diakses_blm_login(self):
        response = Client().get("/Kabar!/")
        self.assertRedirects(response, '/accounts/SignIn/?next=/Kabar%21/')
    
    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve("/Kabar!/")
        self.assertEqual(found.func, homepage)

    def test_apakah_url_dapat_diakses_ketika_sudah_login_oleh_user1(self):
        people = Client()
        people.login(username = "testuser1", password = "1X<ISRUkw+tuK")
        response = people.get("/Kabar!/")
        objectBaru2 = coreDatabase.objects.create(kabarUser = "TESTING")
        jumlahObjectYangAda = coreDatabase.objects.all().count()
        today = timezone.now()
        # Test untuk apakah website ada
        self.assertEqual(response.status_code, 200)
        # test apakah nama user muncul dilayar
        self.assertContains(response, "testuser1")
        # test apakah object yang dibuat ada
        self.assertEqual(jumlahObjectYangAda, 1)
        # test apakah ada tulisan halo (user), apa kabar?
        self.assertContains(response, "Halo testuser1, apa kabar?")
        # test apakah ada form untuk mengisi kabar
        self.assertContains(response, '<form')
        # test apakah ada tombol submit
        self.assertContains(response, 'type="submit"')
        # test apakah halaman ter-redirect setelah user memasukan inputan kabar
        test = 'Unknown'
        response_post = people.post("/Kabar!/", {"Kabar" : test})
        today = timezone.now()
        kabar = coreDatabase.objects.first()
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(kabar.waktu.date(),today.date())

    def test_apakah_url_dapat_diakses_ketika_sudah_login_oleh_user2(self):
        people = Client()
        people.login(username = "testuser2", password = "2HJ1vRV0Z&3iD")
        response = people.get("/Kabar!/")
        objectBaru2 = coreDatabase.objects.create(kabarUser = "TESTING")
        jumlahObjectYangAda = coreDatabase.objects.all().count()
        today = timezone.now()
        # Test untuk apakah website ada
        self.assertEqual(response.status_code, 200)
        # test apakah nama user muncul dilayar
        self.assertContains(response, "testuser2")
        # test apakah object yang dibuat ada
        self.assertEqual(jumlahObjectYangAda, 1)
        # test apakah ada tulisan halo (user), apa kabar?
        self.assertContains(response, "Halo testuser2, apa kabar?")
        # test apakah ada form untuk mengisi kabar
        self.assertContains(response, '<form')
        # test apakah ada tombol submit
        self.assertContains(response, 'type="submit"')
        # test apakah halaman ter-redirect setelah user memasukan inputan kabar
        test = 'Unknown'
        response_post = people.post("/Kabar!/", {"Kabar" : test})
        today = timezone.now()
        kabar = coreDatabase.objects.first()
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(kabar.waktu.date(),today.date())

class functionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome("./chromedriver" ,chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_websit(self):
        self.browser.get(self.live_server_url + '/Kabar!/')
        self.assertIn('<form class="signin-form"', self.browser.page_source)
        self.assertIn('Sign In', self.browser.page_source)
        self.browser.find_element_by_name("buttonsignup").click()
        time.sleep(5)
        self.browser.find_element_by_name("username").send_keys("naufalirfandi")
        time.sleep(3)
        self.browser.find_element_by_name("password1").send_keys("irfandi123")
        time.sleep(3)
        self.browser.find_element_by_name("password2").send_keys("irfandi123")
        time.sleep(3)
        self.browser.find_element_by_name("submitbutton").click()
        time.sleep(3)
        self.assertIn('Halo naufalirfandi, apa kabar?', self.browser.page_source)
        self.assertIn('<div class="table-responsive"', self.browser.page_source)
        self.browser.find_element_by_name("buttonlogout").click()
        time.sleep(5)
        self.assertIn('<form class="signin-form"', self.browser.page_source)
        self.browser.find_element_by_name("username").send_keys("naufalirfandi")
        time.sleep(3)
        self.browser.find_element_by_name("password").send_keys("irfandi123")
        time.sleep(3)
        self.browser.find_element_by_name("tombolsignin").click()
        time.sleep(5)
        self.assertIn('Halo naufalirfandi, apa kabar?', self.browser.page_source)
        self.assertIn('<div class="table-responsive"', self.browser.page_source)
        self.browser.find_element_by_name("buttonlogout").click()
        time.sleep(5)
        self.assertIn('<form class="signin-form"', self.browser.page_source)