from django import forms

class formKabar(forms.Form):
    Kabar = forms.CharField(widget = forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Kabar',
        'type' : 'text',
        'required' : True,
        'maxlength' : 300
    }))