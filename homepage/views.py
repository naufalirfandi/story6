from django.shortcuts import render, redirect
from . import forms, models
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url = "/accounts/SignIn/")
def homepage(request):
    if(request.method == "POST"):
        tmp = forms.formKabar(request.POST)
        if(tmp.is_valid()):
            tmp2 = models.coreDatabase()
            tmp2.kabarUser = tmp.cleaned_data["Kabar"]
            tmp2.pengguna = request.user
            tmp2.save()
        return redirect("/Kabar!")
    else:
        tmp = forms.formKabar()
        tmp2 = models.coreDatabase.objects.all().filter(pengguna = request.user)
        tmp_dictio = {
            'formKabar' : tmp,
            'coreDatabase' : tmp2
        }
        return render(request, 'homepage/homepage.html', tmp_dictio)